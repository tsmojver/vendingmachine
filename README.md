# README #

* Welcome to the Virtual Vending Machine windows console application by Tim Smojver
* This application will allow a user to perform basic operations against vending machine via command line
* This includes but not limited to:
  * Displaying Products
  * Accepting Coins
  * Purchasing Products
  * Receiving Change

### What is this repository for? ###

* Managing the source code and change tracking for the vending machine.
* Version 1.0

### How do I get set up? ###

* Visual Studio 2015 Community
* .NET Framework 4.5.2 or higher
* How to run tests from command line
  * Open a visual studio command prompt
  * Run vendingmachine\BuildAndRunTests.bat
* Resharper (not required but highly recommended)
* To Build: Open VendingMachine.sln and run the visual studio build command (F6 shortcut)
* To Run in Visual Studio: Set VendingMachine.Console as the start up project and choose the build + run command (F5 shortcut)
* In the console app type h for a list of available commands
* To Run the tests using Resharper:
  * From the menu choose Resharper -> Windows -> Unit Test
  * In the test dialog choose run all    

### Concepts ###
* The console app has the following concepts that map nicely to concrete types managed by the CommandManager class
  * Command
    * Single operations such as "accept a coin", "display status"
    * These are the most basic commands that the console app understands
  * Demo
    * A sequence of operations such as "accept coin", "purchase item", "display returned change"  
    * There is a command run the two basic demo's that are included  
  * Commands and Demos can be added to the respective list in the CommandManager class

### Contribution guidelines ###

* Adding Tests
  * Go to the VendingMachine.Implementation project
  * Add a new Class file (class.cs)
  * Give the class a meaningful name. The file name should match the class name.
  * Derive from the IntegrationTestBase class
    * Read the documentation of that class to use it properly
    * If behavior changes are needed that fit with the class's responsibility, such as the ability to override a behavior, make the changes to the class as neeed.
    * Otherwise consider adding a new class with the new responsbility
    * After making changes run the tests periodically to confirm there are no breaking changes. 
      * If there are breaking changes then fix them.
      * Test contracts should not change unless there is good reason.
      * If changes to a test are needed consult with the project owner. 
* For bugs and enhancements
  * Create branch with descriptive name.
  * Add changes along with a supporting test(s).
  * Push the branch to the repo and the code will be reviewed within 1 week. 
  * If the feature is urgent email the owner. 
  * Always add a failing test before making behavior changes.
* Comments
  * All public facing API's. 
  * Implementations are optional unless there is significant complexity worth noting.
  * Should be concise and easily understood. If this is hard to do then chances are the requirements may not be well understood.
  * Follow existing conventions based on existing comments in place. Confirm any deviations with owner before sticking to it.
* Misc
  * Always follow good coding practices
    * This is includes and not limited to the S.O.L.I.D principles
    * Use good judgement when applying these principles and always Keep It Simple.   

### Who do I talk to? ###

* Tim Smojver
* tsmojver@gmail.com
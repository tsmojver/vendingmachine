﻿using System.Collections.Generic;
using VendingMachine.API;
using VendingMachine.Implementation;

namespace VendingMachine.DataSources
{
    /// <summary>
    /// Factory for providing data for a vending machine.
    /// </summary>
    public static class DataFactory
    {
        /// <summary>
        /// Gets list of <see cref="IProduct"/>
        /// </summary>
        /// <returns>A new instance.</returns>
        public static IList<IProduct> GetProducts()
        {
            var products = new List<IProduct>
            {
                new Product
                {
                    Id = 1,
                    Description = "Cola",
                    Price = 1m,
                    Quantity = 1
                },
                new Product
                {
                    Id = 2,
                    Description = "Chips",
                    Price = .5m,
                    Quantity = 10
                },
                new Product
                {
                    Id = 3,
                    Description = "Candy",
                    Price = .65m,
                    Quantity = 1
                },
            };
            return products;
        }

        /// <summary>
        /// Gets list of <see cref="ICoin"/>
        /// </summary>
        /// <returns>A new instance.</returns>
        public static IList<ICoin> GetCoins(int nickelQuantity = 5)
        {
            /*Data taken from https://www.usmint.gov/learn/coin-and-medal-programs/coin-specifications*/
            var coins = new List<ICoin>
            {
                new Coin
                {
                    Weight = 2.5m,
                    Diameter = 19.05m,
                    Thickness = 1.52m,
                    Value = .01m,
                    Accepted = false,
                    CoinType = CoinType.Penny
                },
                new Coin
                {
                    Weight = 5m,
                    Diameter = 21.21m,
                    Thickness = 1.95m,
                    Value = .05m,
                    Accepted = true,
                    CoinType = CoinType.Nickel,
                    Quantity = nickelQuantity
                },
                new Coin
                {
                    Weight = 2.268m,
                    Diameter = 17.91m,
                    Thickness = 1.35m,
                    Value = .1m,
                    Accepted = true,
                    CoinType = CoinType.Dime,
                    Quantity = 5
                },
                new Coin
                {
                    Weight = 5.670m,
                    Diameter = 24.26m,
                    Thickness = 1.75m,
                    Value = .25m,
                    Accepted = true,
                    CoinType = CoinType.Quarter,
                    Quantity = 5
                }
            };
            return coins;
        }
    }
}

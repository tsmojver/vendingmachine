﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using VendingMachine.API;
using VendingMachine.DataSources;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Tests to confirm the change making requirements
    /// </summary>
    [TestFixture]
    public class MakeChangeIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override IList<ICoin> GetCoins()
        {
            return DataFactory.GetCoins(nickelQuantity:0);
        }

        [Test]
        public void ShouldReturnTwoQuartersWhenMakingChange()
        {
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Dime);
            AcceptCoin(CoinType.Dime);
            AcceptCoin(CoinType.Dime);
            VendingMachineService.Dispense(2);
            Assert.AreEqual("EXACT CHANGE ONLY",VendingMachineService.Display);
        }

        [Test]
        public void ShouldMakeChange()
        {
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            VendingMachineService.Dispense(2);
            Assert.AreEqual(1, VendingMachineService.ReturnedCoins.Count);
            Assert.AreEqual(1, VendingMachineService.ReturnedCoins.Count(c => ((Coin)c).CoinType == CoinType.Quarter));
        }
    }
}

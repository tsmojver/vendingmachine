﻿using System;
using NUnit.Framework;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Tests to confirm the Accept Coin requirements
    /// </summary>
    [TestFixture]
    public class AcceptCoinIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        protected override void Initialize()
        {
            base.Initialize();
        }

        [Test, Order(1)]
        public void ShouldDisplayInsertCoin()
        {
            Assert.AreEqual("INSERT COIN", VendingMachineService.Display);
        }

        [Test]
        [TestCase(CoinType.Nickel)]
        [TestCase(CoinType.Dime)]
        [TestCase(CoinType.Quarter)]
        public void ShouldAcceptValidCoins(CoinType coinType)
        {
            var coin = GetCoin(coinType);
            AcceptCoin(coin);
            Assert.AreEqual(string.Format("CURRENT AMOUNT: {0:c}", coin.Value), VendingMachineService.Display);
        }

        [Test]
        [TestCase(CoinType.Penny)]
        public void ShouldRejectInvalidCoins(CoinType coinType)
        {
            var coin = GetCoin(coinType);
            AcceptCoin(coin);
            Assert.AreEqual(coin.Description + " NOT ACCEPTED", VendingMachineService.Display);
        }
    }
}

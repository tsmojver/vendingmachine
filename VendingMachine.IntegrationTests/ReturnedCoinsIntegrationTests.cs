﻿using System.Linq;
using NUnit.Framework;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Tests to confirm the returned coin requirements
    /// </summary>
    [TestFixture]
    public class ReturnedCoinsIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        protected override void Initialize()
        {
            base.Initialize();
        }

        [Test]
        public void ShouldReturnAllCoinsWhenAsked()
        {
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            VendingMachineService.ReturnCoins();
            Assert.AreEqual(3, VendingMachineService.ReturnedCoins.Count(c=>((Coin)c).CoinType == CoinType.Quarter));
        }
        
    }
}

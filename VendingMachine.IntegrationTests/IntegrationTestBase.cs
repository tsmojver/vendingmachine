﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.API;
using VendingMachine.DataSources;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Base class for performing integration tests.
    /// </summary>
    /// <remarks>
    /// Creating dervied instance of this class will make it easy to add new tests for the vending machine.
    /// The default behavior will allow tests to be run against a default set of data.
    /// To provide specific data such as a list of <see cref="ICoin"/>, override <see cref="GetCoin"/> and return the desired collection.
    /// </remarks>
    public class IntegrationTestBase
    {
        private IVendingMachineService _vendingMachineService;
        private IList<ICoin> _coins;

        /// <summary>
        /// Provides override-able default initialization behavoir
        /// </summary>
        protected virtual void Initialize()
        {
            _coins = GetCoins();
            _vendingMachineService = 
                new VendingMachineService(new CoinFactory(_coins), 
                new ProductService(DataFactory.GetProducts()), 
                new ChangeService(_coins.Where(c=>c.Accepted).ToList()));
        }

        /// <summary>
        /// Gets list of coins for testing.
        /// </summary>
        /// <returns></returns>
        protected virtual IList<ICoin> GetCoins()
        {
            return DataFactory.GetCoins();
        }

        /// <summary>
        /// Gets the vending machine service that was initialized for the current test.
        /// </summary>
        protected IVendingMachineService VendingMachineService
        {
            get
            {
                return this._vendingMachineService;;
            }  
        }

        /// <summary>
        /// Gets the coins collection that was initialized for the current test.
        /// </summary>
        protected IList<ICoin> Coins
        {
            get
            {
                return this._coins;
            }  
        } 
        
        /// <summary>
        /// Helper method for getting coins based on known type.
        /// </summary>
        /// <param name="coinType">the coin type.</param>
        /// <returns>An <see cref="ICoin"/> instance.</returns>
        protected ICoin GetCoin(CoinType coinType)
        {
            return _coins.Single(c => ((Coin)c).CoinType == coinType);
        }

        /// <summary>
        /// Helper method for calling <see cref="IVendingMachineService.AcceptCoin"/>
        /// </summary>
        /// <param name="coinType">the coin type</param>
        protected void AcceptCoin(CoinType coinType)
        {
            var coin = GetCoin(coinType);
            AcceptCoin(coin);
        }

        /// <summary>
        /// Helper method for calling <see cref="IVendingMachineService.AcceptCoin"/>
        /// </summary>
        /// <param name="coin">the coin instance</param>
        protected void AcceptCoin(ICoin coin)
        {
            VendingMachineService.AcceptCoin(coin.Weight, coin.Diameter, coin.Thickness);
        }
    }
}

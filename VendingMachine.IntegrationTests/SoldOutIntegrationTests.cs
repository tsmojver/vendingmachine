﻿using NUnit.Framework;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Tests to confirm the Sold Out requirements
    /// </summary>
    [TestFixture]
    public class SoldOutCoinsIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        protected override void Initialize()
        {
            base.Initialize();
        }

        [Test]
        public void ShouldReturnAllCoinsWhenAsked()
        {
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            VendingMachineService.Dispense(1);
            Assert.AreEqual("THANK YOU", VendingMachineService.Display);
            VendingMachineService.Dispense(1);
            Assert.AreEqual("SOLD OUT", VendingMachineService.Display);
        }
        
    }
}

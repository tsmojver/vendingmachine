﻿using System;
using NUnit.Framework;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Tests to confirm the Selected Product requirements
    /// </summary>
    [TestFixture]
    public class SelectProductIntegrationTests : IntegrationTestBase
    {
        [SetUp]
        protected override void Initialize()
        {
            base.Initialize();
        }

        [Test, Order(1)]
        public void ShouldDispenseProductWhenFullAmountAdded()
        {
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Quarter);
            Assert.AreEqual(1, VendingMachineService.CurrentAmount);
            VendingMachineService.Dispense(1);
            Assert.AreEqual(0, VendingMachineService.CurrentAmount);
            Assert.AreEqual("THANK YOU", VendingMachineService.Display);
            Assert.AreEqual("INSERT COIN", VendingMachineService.Display);
        }

        [Test, Order(2)]
        public void ShouldDisplayPriceWhenCurrentAmountLessThanProductPrice()
        {
            AcceptCoin(CoinType.Quarter);
            VendingMachineService.Dispense(1);
            Assert.AreEqual(string.Format("PRICE: {0:c}", 1m), VendingMachineService.Display);
            Assert.AreEqual("INSERT COIN", VendingMachineService.Display);
            VendingMachineService.Dispense(1);
            Assert.AreEqual(string.Format("PRICE: {0:c}", 1m), VendingMachineService.Display);
            Assert.AreEqual("INSERT COIN", VendingMachineService.Display);
        }
    }
}

﻿using System.Collections.Generic;
using NUnit.Framework;
using VendingMachine.API;
using VendingMachine.DataSources;
using VendingMachine.Implementation;

namespace VendingMachine.IntegrationTests
{
    /// <summary>
    /// Tests to confirm the change making requirements
    /// </summary>
    [TestFixture]
    public class MakeChangeWhenUserAmountCanBeUsedIntegrationTest : IntegrationTestBase
    {
        [SetUp]
        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override IList<ICoin> GetCoins()
        {
            return DataFactory.GetCoins(nickelQuantity:0);
        }

        [Test]
        public void ShouldReturnTwoQuartersWhenMakingChange()
        {
            AcceptCoin(CoinType.Quarter);
            AcceptCoin(CoinType.Dime);
            AcceptCoin(CoinType.Dime);
            AcceptCoin(CoinType.Dime);
            VendingMachineService.Dispense(2);
            Assert.AreEqual("EXACT CHANGE ONLY",VendingMachineService.Display);
        }
    }
}

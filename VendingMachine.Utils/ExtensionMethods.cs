﻿using System;
using System.Text;

namespace VendingMachine.Utils
{
    public static class StringExtensions
    {
        public static string AppendCharacters(this String str, string character, int length)
        {
            var sb = new StringBuilder();
            sb.Append(str);
            for (int i = 0; i < length - str.Length; i++)
            {
                sb.Append(character);
            }
            return sb.ToString();
        }
    }
}

﻿using System;
using VendingMachine.API;

namespace VendingMachine.Implementation
{
    public class Product : IProduct
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public override string ToString()
        {
            return string.Format("Number: {0} Description: {1}  Price:{2:C} Quantity:{3}", Id, Description, Price,
                Quantity);
        }
    }
}
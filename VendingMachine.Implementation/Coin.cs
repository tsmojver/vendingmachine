using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using VendingMachine.API;

namespace VendingMachine.Implementation
{
    public class Coin : ICoin
    {
        public string Description
        {
            get
            {
                return CoinType.Description();
            }  
        } 

        public decimal Weight { get; set; }
        public decimal Diameter { get; set; }
        public decimal Thickness { get; set; }
        public decimal Value { get; set; }
        public bool Accepted { get; set; }
        public int Quantity { get; set; }
        public void DecrementQuantity()
        {
            Quantity = Quantity > 0 ? Quantity -= 1 : Quantity;
        }

        public CoinType CoinType { get; set; }
    }

    public static class EnumerationExtension
    {
        public static string Description(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            
            return attributes.Any() ? ((DescriptionAttribute)attributes.ElementAt(0)).Description : null;
        }
    }
}
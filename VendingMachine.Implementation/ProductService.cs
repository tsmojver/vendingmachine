using System.Collections.Generic;
using System.Linq;
using VendingMachine.API;

namespace VendingMachine.Implementation
{
    public class ProductService : IProductService
    {
        public ProductService(IList<IProduct> products)
        {
            Products = products;
        }
        public IList<IProduct> Products { get; private set; }
        public void DecrementProductQuantity(int productId)
        {
            var product = Products.SingleOrDefault(p => p.Id == productId);
            if (product != null && product.Quantity > 0)
            {
                product.Quantity -= 1;
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VendingMachine.API;

namespace VendingMachine.Implementation
{
    public class VendingMachineService : IVendingMachineService
    {
        private readonly ICoinFactory _coinFactory;
        private readonly IProductService _productService;
        private readonly IChangeService _changeService;
        private IList<ICoin> _coins = new List<ICoin>();
        private IList<ICoin> _returnedCoins = new List<ICoin>();

        public VendingMachineService(ICoinFactory coinFactory, IProductService productService, IChangeService changeService)
        {
            _coinFactory = coinFactory;
            _productService = productService;
            _changeService = changeService;
        }

        private string _display;
        public string Display
        {
            get
            {
                if (!string.IsNullOrEmpty(_display))
                {
                    var temp = _display;
                    _display = null; 
                    return temp;
                }

                return "INSERT COIN";
            }
            set { _display = value; }
        }

        public void AcceptCoin(decimal weight, decimal diameter, decimal thickness)
        {
            var coin = _coinFactory.GetCoin(weight, diameter, thickness);
            if (coin == null)
            {
                Display = "Unrecognized Coin";
            }
            else if (coin.Accepted)
            {
                _coins.Add(coin);
                Display = string.Format("CURRENT AMOUNT: {0:c}", CurrentAmount);
            }
            else
            {
                Display = string.Format("{0} NOT ACCEPTED", coin.Description);
                ReturnedCoins.Add(coin);
            }
        }

        public string Products
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var product in _productService.Products)
                {
                    sb.AppendLine(product.ToString());
                }
                return sb.ToString();
            }
        }

        public void ReturnCoins()
        {
            var temp = this._coins;
            this._returnedCoins.Clear();
            this._coins = this._returnedCoins;
            this._returnedCoins = temp;
        }

        public IList<ICoin> ReturnedCoins
        {
            get { return _returnedCoins; }  
        } 

        public void Dispense(int productId)
        {
            var product = _productService.Products.SingleOrDefault(p => p.Id == productId);
            if (product == null)
            {
                Display = "Invalid Product";
            }
            else if (product.Quantity <= 0)
            {
                Display = "SOLD OUT";
            }
            else if (CurrentAmount == product.Price)
            {
                _productService.DecrementProductQuantity(productId);
                _coins.Clear();
                Display = "THANK YOU";
            }
            else if (CurrentAmount >= product.Price)
            {
                if (_changeService.TryMakeChange(product.Price, CurrentAmount, ReturnedCoins))
                {
                    _productService.DecrementProductQuantity(productId);
                    _coins.Clear();
                    Display = "THANK YOU";
                }
                else
                {
                    Display = "EXACT CHANGE ONLY";
                }
            }
            else
            {
                Display = string.Format("PRICE: {0:C}", product.Price);
            }
        }

        public decimal CurrentAmount
        {
            get
            {
                return _coins.Sum(c => c.Value);
            }
        }
    }
}
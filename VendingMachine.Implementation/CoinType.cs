using System;
using System.ComponentModel;
using System.Reflection;

namespace VendingMachine.Implementation
{
    public enum CoinType
    {
        [Description("Penny")]
        Penny,
        [Description("Nickel")]
        Nickel,
        [Description("Dime")]
        Dime,
        [Description("Quarter")]
        Quarter
    }
}
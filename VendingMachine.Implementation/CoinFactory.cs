﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.API;

namespace VendingMachine.Implementation
{
    public class CoinFactory : ICoinFactory
    {
        private readonly IList<ICoin> _coins;

        public CoinFactory(IList<ICoin> coins)
        {
            _coins = coins;
        }

        public ICoin GetCoin(decimal weight, decimal diameter, decimal thickness)
        {
            return _coins.SingleOrDefault(c => c.Weight == weight && c.Diameter == diameter && c.Thickness == thickness);
        }
    }
}
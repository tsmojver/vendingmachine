using System.Collections.Generic;
using System.Linq;
using VendingMachine.API;

namespace VendingMachine.Implementation
{
    public class ChangeService : IChangeService
    {
        private readonly IList<ICoin> _coins;

        public ChangeService(IList<ICoin> coins)
        {
            _coins = coins;
        }

        public bool TryMakeChange(decimal lowerPrice, decimal upperPrice, IList<ICoin> returnedCoins)
        {
            returnedCoins.Clear();
            var difference = (upperPrice - lowerPrice) * 100;
            foreach (var coin in _coins.OrderByDescending(c=>c.Value))
            {
                if (coin.Quantity <= 0)
                {
                    continue;
                }

                var val = coin.Value*100;
                while (difference > 0 && difference % val == 0)
                {
                    difference -= val;
                    returnedCoins.Add(coin);
                    coin.DecrementQuantity();
                }
            }
            return returnedCoins.Sum(c => c.Value) == upperPrice - lowerPrice;
        }
    }
}
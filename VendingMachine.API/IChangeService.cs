using System.Collections.Generic;

namespace VendingMachine.API
{
    /// <summary>
    /// Provides services related to making change based on prices and coin inventory.
    /// </summary>
    public interface IChangeService
    {
        /// <summary>
        /// Produces coins to be returned based on the difference in two prices
        /// </summary>
        /// <param name="lowerPrice">the price to be subtracted from</param>
        /// <param name="upperPrice">the price to subtract</param>
        /// <param name="coins">collection of coins to be updated</param>
        /// <returns>
        /// true if change can be made based on internal coin availability, otherwise false.
        /// if true, the coins collection will contain a list of zero or more coins and will not be null.
        /// </returns>
        bool TryMakeChange(decimal lowerPrice, decimal upperPrice, IList<ICoin> coins);
    }
}
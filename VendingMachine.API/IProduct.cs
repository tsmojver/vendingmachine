namespace VendingMachine.API
{
    /// <summary>
    /// Represents a Product that can be purchased.
    /// </summary>
    public interface IProduct
    {
        /// <summary>
        /// Gets the products unique identifier.
        /// </summary>
        int Id { get; }
        /// <summary>
        /// Gets the user-friendly display name.
        /// </summary>
        string Description { get; }
        /// <summary>
        /// Gets the price in U.S currency. 
        /// </summary>
        /// <remarks>
        /// Penny = .01
        /// Quarter = .25
        /// </remarks>
        decimal Price { get; }
        /// <summary>
        /// Gets the quantity on hand
        /// </summary>
        int Quantity { get; set; }
    }
}
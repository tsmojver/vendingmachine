namespace VendingMachine.API
{
    /// <summary>
    /// Represents a coin
    /// </summary>
    public interface ICoin
    {
        /// <summary>
        /// Gets the descrition. Can be displayed to end user.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the weight in grams.
        /// </summary>
        decimal Weight { get; }

        /// <summary>
        /// Gets the diameter in millimeters
        /// </summary>
        decimal Diameter { get;}

        /// <summary>
        /// Gets the thickness in millimeters
        /// </summary>
        decimal Thickness { get; }

        /// <summary>
        /// Gets the value. 
        /// </summary>
        /// <remarks>
        /// Penny = 0.01
        /// Quarter = .025
        /// </remarks>
        decimal Value { get; }

        /// <summary>
        /// Gets the Accepted value.
        /// </summary>
        /// <remarks>
        /// When true the vending machine will count the value towards the purchase of the item.
        /// </remarks>
        bool Accepted { get; }

        /// <summary>
        /// Gets the Quanitity on hand.
        /// </summary>
        int Quantity { get; }

        /// <summary>
        /// Decrements the <see cref="Quantity"/> on hand.
        /// </summary>
        /// <remarks>
        /// When <see cref="Quantity"/> equals 0 the method will do nothing and just return.
        /// </remarks>
        void DecrementQuantity();
    }
}
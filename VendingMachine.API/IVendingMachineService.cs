﻿using System.Collections.Generic;

namespace VendingMachine.API
{
    /// <summary>
    /// Provides services for operating a vending machine.
    /// </summary>
    public interface IVendingMachineService
    {
        /// <summary>
        /// Gets the user-friendly message to be displayed.
        /// </summary>
        /// <remarks>        
        /// This should be displayed to the user after each operation is called as the value will changed based on the operation.
        /// </remarks>
        string Display { get; }
        /// <summary>
        /// Accepts user inserted coins.
        /// </summary>
        /// <param name="weight">the weight of the coin in grams.</param>
        /// <param name="diameter">the diameter of the coin in millimeters.</param>
        /// <param name="thickness">the thickness of the coin in millimeters.</param>
        /// <remarks>
        /// If coin is not accepted it will be placed in the <see cref="ReturnedCoins"/> collection.
        /// </remarks>
        void AcceptCoin(decimal weight, decimal diameter, decimal thickness);
        /// <summary>
        /// Gets the list of available products to purchase.
        /// </summary>
        string Products { get; }
        /// <summary>
        /// Attempts to dispense an item.
        /// </summary>
        /// <param name="productId">the <see cref="IProduct.Id"/> to purchase.</param>
        /// <remarks>
        /// After calling this method display the <see cref="Display"/> to the user.
        /// </remarks>
        void Dispense(int productId);
        /// <summary>
        /// Returns coins that the user has added but not redeemed, if any.
        /// </summary>
        /// <remarks>
        /// After calling this method get the coins from <see cref="ReturnedCoins"/>
        /// </remarks>
        void ReturnCoins();
        /// <summary>
        /// Gets the returned coins to the user.
        /// </summary>
        /// <remarks>
        /// This is updated when <see cref="ReturnedCoins"/>, <see cref="Dispense"/>, and <see cref="AcceptCoin"/> is called.
        /// </remarks>
        IList<ICoin> ReturnedCoins { get; }
        /// <summary>
        /// Gets the sum of coins added by user that have not been returned or redeemed.
        /// </summary>
        decimal CurrentAmount { get; }
    }
}
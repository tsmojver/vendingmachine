﻿namespace VendingMachine.API
{
    /// <summary>
    /// Gets <see cref="ICoin"/> instances.
    /// </summary>
    public interface ICoinFactory
    {
        /// <summary>
        /// Get an <see cref="ICoin"/>
        /// </summary>
        /// <param name="weight">the coin weight in grams</param>
        /// <param name="diameter">the coin diameter in millimeters</param>
        /// <param name="thickness">the coin thickness in millimeters</param>
        /// <returns>
        /// An instance of <see cref="ICoin"/> if recognized by system, otherwise null.</returns>
        ICoin GetCoin(decimal weight, decimal diameter, decimal thickness);
    }
}
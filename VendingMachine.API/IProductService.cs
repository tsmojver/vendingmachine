using System.Collections.Generic;

namespace VendingMachine.API
{
    /// <summary>
    /// Provides services for working with <see cref="IProduct"/>
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Gets the list of products that can be purchased.
        /// </summary>
        IList<IProduct> Products { get; }

        /// <summary>
        /// Decrements the quanity of a product.
        /// </summary>
        /// <param name="productId"></param>
        /// <remarks>
        /// When <see cref="IProduct.Quantity"/> equals 0 the method will do nothing and just return.
        /// </remarks>
        void DecrementProductQuantity(int productId);
    }
}
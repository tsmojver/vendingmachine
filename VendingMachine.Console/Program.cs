﻿using System;
using System.Linq;
using System.Reflection;
using VendingMachine.DataSources;
using VendingMachine.Implementation;
using VendingMachine.Utils;

namespace VendingMachine.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize Dependencies
            var coins = DataFactory.GetCoins();
            var vendingMachineService =
                new VendingMachineService(new CoinFactory(DataFactory.GetCoins()),
                    new ProductService(DataFactory.GetProducts()),
                    new ChangeService(coins.Where(c => c.Accepted).ToList()));
            var commandManager = new CommandManager(vendingMachineService);
            
            DisplayHeaderInformation();

            //Begin main loop for command processing
            var currentCommand = commandManager.GetCommand(System.Console.ReadLine());
            while (currentCommand.Id != "quit")
            {
                if (currentCommand.Execute != null)
                {
                    currentCommand.Execute();
                }
                else
                {
                    System.Console.WriteLine(currentCommand.Description);
                }

                currentCommand = commandManager.GetCommand(System.Console.ReadLine());
            }
        }

        private static void DisplayHeaderInformation()
        {
            var assembly = Assembly.GetExecutingAssembly();
            
            var descriptionAttribute =(AssemblyDescriptionAttribute) assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute)).First();
            var authorAttribute = (AssemblyCompanyAttribute) assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute)).First();
            
            var border = "******************************************************";
            System.Console.WriteLine(border);
            System.Console.WriteLine("*".AppendCharacters(" ", border.Length-1) + "*");
            System.Console.WriteLine(String.Format("* Welcome The {0}!", descriptionAttribute.Description).AppendCharacters(" ", border.Length - 1) + "*");
            System.Console.WriteLine(String.Format("* Author: {0}", authorAttribute.Company).AppendCharacters(" ", border.Length - 1) + "*");
            System.Console.WriteLine(String.Format("* Version: {0}", assembly.GetName().Version).AppendCharacters(" ", border.Length - 1) + "*");
            System.Console.WriteLine("*".AppendCharacters(" ", border.Length - 1) + "*");
            System.Console.WriteLine(border);

            System.Console.WriteLine("");
            System.Console.WriteLine("Type h for a list of available commands");
            System.Console.WriteLine("");
        }
    }
}

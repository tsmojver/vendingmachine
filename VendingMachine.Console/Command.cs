﻿using System;
using VendingMachine.Utils;

namespace VendingMachine.Console
{
    /// <summary>
    /// Represents a single command against the vending machine, such as accepting a coin, dispalying help, etc
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Gets or sets unique id of the command
        /// </summary>
        public String Id { get; set; }

        /// <summary>
        /// Gets or sets the user friendly description
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// Gets or sets operation to run
        /// </summary>
        public Action Execute { get; set; }

        /// <summary>
        /// Returns the custom formatted description of the demo
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Id.AppendCharacters(" ", 10) + Description;
        }
    }
}
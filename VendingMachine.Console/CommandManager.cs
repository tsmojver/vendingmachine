using System;
using System.Collections.Generic;
using System.Linq;
using VendingMachine.API;
using VendingMachine.DataSources;
using VendingMachine.Implementation;

namespace VendingMachine.Console
{
    /// <summary>
    /// Provides vending machine commands
    /// </summary>
    public class CommandManager
    {
        private readonly IList<Demo> _demos;
        private readonly IList<Command> _commands;
        private readonly IVendingMachineService _vendingMachineService;

        public CommandManager(IVendingMachineService vendingMachineService)
        {
            _vendingMachineService = vendingMachineService;
            _commands = new List<Command>
            {
                new Command {Id = "h", Description = "Help", Execute = () =>
                {
                    foreach (var c in Commands)
                    {
                        System.Console.WriteLine(c);
                    }
                }},
                new Command {Id = "quit", Description = "Quit"},
                new Command
                {
                    Id = "q",
                    Description = "Add Quarter",
                    Execute = () => { AcceptCoinHelper(CoinType.Quarter); }
                },
                new Command
                {
                    Id = "d",
                    Description = "Add Dime",
                    Execute = () => { AcceptCoinHelper(CoinType.Dime); }
                },
                new Command
                {
                    Id = "n",
                    Description = "Add Nickel",
                    Execute = () => { AcceptCoinHelper(CoinType.Nickel); }
                },
                new Command
                {
                    Id = "p",
                    Description = "Add Penny",
                    Execute = () => { AcceptCoinHelper(CoinType.Quarter); }
                },
                new Command
                {
                    Id = "pl",
                    Description = "Display Product List",
                    Execute = () =>
                    {
                        System.Console.WriteLine(_vendingMachineService.Products);
                    }
                },
                new Command
                {
                    Id = "p1",
                    Description = "Purchase Product 1",
                    Execute = () =>
                    {
                        vendingMachineService.Dispense(1);
                    }
                },
                new Command
                {
                    Id = "p2",
                    Description = "Purchase Product 2",
                    Execute = () =>
                    {
                        vendingMachineService.Dispense(2);
                    }
                },
                new Command
                {
                    Id = "p3",
                    Description = "Purchase Product 3",
                    Execute = () =>
                    {
                        vendingMachineService.Dispense(3);
                    }
                },
                new Command
                {
                    Id = "r",
                    Description = "Return Coins",
                    Execute = () =>
                    {
                        vendingMachineService.ReturnCoins();
                    }
                },
                new Command
                {
                    Id = "drc",
                    Description = "Display Return Coins",
                    Execute = () =>
                    {
                        foreach (var c in vendingMachineService.ReturnedCoins)
                        {
                            System.Console.WriteLine(c.Description);
                        }
                    }
                },
                new Command
                {
                    Id = "ds",
                    Description = "Display Status",
                    Execute = () =>
                    {
                        System.Console.WriteLine(vendingMachineService.Display);
                    }
                },
                new Command
                {
                    Id = "ca",
                    Description = "Display Current Amount",
                    Execute = () =>
                    {
                        System.Console.WriteLine(vendingMachineService.CurrentAmount);
                    }
                },
                new Command
                {
                    Id = "dd",
                    Description = "Display Demo's",
                    Execute = () =>
                    {
                        foreach (var d in _demos)
                        {
                            System.Console.WriteLine(d);
                        }
                    }
                },
                new Command
                {
                    Id = "d1",
                    Description = "Run Demo 1",
                    Execute = () =>
                    {
                        var demo = _demos.Single(d => d.Id == "1");
                        foreach (var c in demo.Commands)
                        {
                            c.Execute();
                        }
                    }
                },
                new Command
                {
                    Id = "d2",
                    Description = "Run Demo 1",
                    Execute = () =>
                    {
                        var demo = _demos.Single(d => d.Id == "2");
                        foreach (var c in demo.Commands)
                        {
                            c.Execute();
                        }
                    }
                }

            };

            var displayProductDemo = new Demo
            {
                Id = "1",
                Description = "Displays list of products",
                Commands = new List<Command>()
            };
            displayProductDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "pl"));

            var simplePurchaseDemo = new Demo
            {
                Id = "2",
                Description = "Purchase an item and receive change.",
                Commands = new List<Command>()
            };
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c=>c.Id == "q"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "ds"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "ds"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "q"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "ds"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "ds"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "p2"));
            simplePurchaseDemo.Commands.Add(_commands.SingleOrDefault(c => c.Id == "ds"));

            _demos = new List<Demo>
            {
                displayProductDemo,
                simplePurchaseDemo
            };
        }
        
        private void AcceptCoinHelper(CoinType coinType)
        {
            var coin = DataFactory.GetCoins().Single(x => ((Coin)x).CoinType == coinType);
            _vendingMachineService.AcceptCoin(coin.Weight, coin.Diameter, coin.Thickness);
        }

        public Command GetCommand(String id)
        {
            return Commands.SingleOrDefault(c => c.Id == id) ?? new Command {Description = "Unknown Commmand. Enter h for a list of available commands"};
        }

        public IList<Command> Commands
        {
            get
            {
                return _commands;
            }
        }
    }
}

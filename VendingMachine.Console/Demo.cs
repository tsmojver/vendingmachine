﻿using System.Collections.Generic;
using VendingMachine.Utils;

namespace VendingMachine.Console
{
    /// <summary>
    /// Represents a demonstration of full user experience.
    /// </summary>
    public class Demo
    {
        /// <summary>
        /// Gets or sets the unique id of the demo
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// Gets or sets the user friendly description
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Gets or sets the  list of commands to run for the demo
        /// </summary>
        public IList<Command> Commands { get; set; }

        /// <summary>
        /// Returns the custom formatted description of the demo
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Id.AppendCharacters(" ", 10) + Description;
        }
    }
}